# APIAnimals

## Introduction

Proyecto back-end de laravel con React orientado a servicios y Mysql como motor de base de datos

```sh
git clone https://gitlab.com/jflorezlondono4/apianimals.git
```

## Installation

```sh
create database freedom

php artisan migrate

php artisan serve
```

## Peticiones
Get

http://127.0.0.1:8000/api/animals

http://127.0.0.1:8000/api/corrals

http://127.0.0.1:8000/api/animalcorral

http://127.0.0.1:8000/api/animalcorral/animalxcorral

http://127.0.0.1:8000/api/animalcorral/cantidad

http://127.0.0.1:8000/api/animalcorral/promedio/1

POST

http://127.0.0.1:8000/api/animals

http://127.0.0.1:8000/api/corrals?limit=20

http://127.0.0.1:8000/api/animalcorral
