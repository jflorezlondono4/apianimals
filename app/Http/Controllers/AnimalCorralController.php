<?php

namespace App\Http\Controllers;

use App\Models\AnimalCorral;
use Illuminate\Http\Request;
use DB;

class AnimalCorralController extends Controller
{
    public function index()
    {
        $animalscorrals = AnimalCorral::all();
        return $animalscorrals;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $animalscorrals = AnimalCorral::create($request->all());
        return $animalscorrals;

    }

    //punto 3. animales que se encuentran en un corral
    public function animalesXCorral()
    {
        
        //->groupBy('usuarios.nombresUsuario', 'asc')
        $cantidad = DB::table('animal_corrals')
                                ->select('animals.name as Nombre animal', 'animal_corrals.cantidad as Cantidad animales','corrals.limit as Capacidad corral', 'corrals.id as Corral numero')
                                    ->join('animals', 'animal_corrals.animal_id', 'animals.id')
                                    ->join('corrals', 'animal_corrals.corral_id', '=', 'corrals.id')
                                    ->get();

        return $cantidad;
    }

    //punto 4. cantidad de animales en corrales
    public function cantidadAnimalesCorral()
    {
        
        
        $cantidad = DB::table('animal_corrals')
                                ->selectRaw('sum(animal_corrals.cantidad) as Cantidad')
                                    ->join('animals', 'animal_corrals.animal_id', 'animals.id')
                                    ->join('corrals', 'animal_corrals.corral_id', '=', 'corrals.id')
                                    ->groupBy('animal_corrals.corral_id')
                                    ->get();

        return $cantidad;
    }

    //Punto 6. Promedio de edad de animales
    public function promedioEdadAnimales(Request $request, $id)
    {
        
        $cantidad = DB::table('animal_corrals')
                                ->selectRaw('AVG(animals.age) as Cantidad')
                                    ->join('animals', 'animal_corrals.animal_id', 'animals.id')
                                    ->join('corrals', 'animal_corrals.corral_id', '=', 'corrals.id')
                                    ->where('corrals.id', '=', $id)
                                    ->get();

        return $cantidad;
    }
}
