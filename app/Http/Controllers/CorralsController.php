<?php

namespace App\Http\Controllers;

use App\Models\Corral;
use Illuminate\Http\Request;

class CorralsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $corrals = Corral::all();
        return $corrals;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $corrals = Corral::create($request->all());
        return $corrals;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Corrals  $corrals
     * @return \Illuminate\Http\Response
     */
    public function show(Corrals $corrals)
    {
        $corral = DB::table('corrals')
                         ->select('cantidad', 'animal_id', 'corral_id')
                            ->where('id', $corrals->input('id'))
                            ->get();

        return $corral;
    }
}