<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Corral extends Model
{
    use HasFactory;

    protected $fillable = ["limit"];

    public function animals()
    {
        return $this->belongsToMany(Animal::class)
            ->withPivot(['expires_at'])
            ->withTimestamps();
    }
}
