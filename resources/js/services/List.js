const baseUrl = "http://localhost:8000/api/animals"
import axios from "axios";
const animalcorral = {};

animalcorral.listAnimals = async () => {
    const urlList = baseUrl
    const res = await axios.get(urlList)
    .then(response=>{ return response.data; })
    .catch(error=>{ return error; })
    return res;
}

export default animalcorral