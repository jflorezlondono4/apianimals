import React, { useEffect, useState } from 'react';
import ReactDOM from 'react-dom';
import serviceListAnimals from '../services/List'
import { Link } from "react-router-dom";


function ListAnimals(){

    const [ listAnimals, setlistAnimals ] = useState([]);
  
    useEffect(()=>{
  
      async function fetchDataAnimals(){
        const res = await serviceListAnimals.listAnimals();
        console.log(res.data);
        setlistAnimals(res.data)
      }
  
      fetchDataAnimals();
  
    },[])
  
    return (
      <section>
        <table class="table">
          <thead class="thead-dark">
            <tr>
              <th scope="col">#</th>
              <th scope="col">Name</th>
              <th scope="col">Age</th>
              <th scope="col">Dangerous</th>
            </tr>
          </thead>
          <tbody>
  
          {
            listAnimals.map((item)=>{
              return(
                <tr>
                  <th scope="row">{item.id}</th>
                  <td>{item.name}</td>
                  <td>{item.age}</td>
                  <td>{item.dangerous}</td>
                </tr>
              )
            })
          }
          
          </tbody>
        </table>
      </section>
    )
  }
  
  export default ListAnimals;