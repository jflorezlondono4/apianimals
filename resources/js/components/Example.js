import React, { useEffect, useState } from 'react';
import ReactDOM from 'react-dom';
import serviceListAnimals from '../services/List'

function Example() {
    return (
        <div className="container">
            <div className="row justify-content-center">
                <div className="col-md-8">
                    <div className="card">
                        <h1 className="card-header">List animals</h1>
                    </div>
                </div>
            </div>
        </div>
    );
}

function ListAnimals(){

    const [ listAnimals, setlistAnimals ] = useState([]);
  
    useEffect(()=>{
  
      async function fetchDataAnimals(){
        const res = await serviceListAnimals.listAnimals();
        console.log(res.data);
        setlistAnimals(res.data)
      }
  
      fetchDataAnimals();
  
    },[])
  
    return (
      <section>
        <table class="table">
          <thead class="thead-dark">
            <tr>
              <th scope="col">#</th>
              <th scope="col">Name</th>
              <th scope="col">Age</th>
              <th scope="col">Dangerous</th>
            </tr>
          </thead>
          <tbody>
  
          {

          }
          
          </tbody>
        </table>
      </section>
    )
  }

export default Example;

if (document.getElementById('example')) {
    ReactDOM.render(<Example />, document.getElementById('example'));
    ReactDOM.render(<ListAnimals />, document.getElementById('list'));
}
