<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


//Animals
Route::apiResource("animals", "AnimalsController");
Route::get("animals/list", "AnimalsController@list");

//Corrals
Route::apiResource("corrals", "CorralsController");

//AnimalCorrals
Route::get("animalcorral/animalxcorral", "AnimalCorralController@animalesXCorral");
Route::get("animalcorral/cantidad", "AnimalCorralController@cantidadAnimalesCorral");
Route::get('animalcorral/promedio/{id}', 'PhotoController@method');
Route::apiResource("animalcorral", "AnimalCorralController");